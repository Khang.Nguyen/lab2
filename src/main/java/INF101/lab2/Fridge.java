package INF101.lab2;

import java.util.ArrayList;
import java.util.List;

public class Fridge implements IFridge{
    
    int max_size = 20; //maks strl av fridge
    ArrayList<FridgeItem> items = new ArrayList<>(); //oppretter en tom items-liste

    public int totalSize(){
        return max_size; //retunerer max-size av fridge (i vår tilfelle 20)
    }
    @Override
    public int nItemsInFridge() {
        return items.size(); //returnerer antall items i fridge
    }
    @Override
    public boolean placeIn(FridgeItem item){ //sjekker om det  er plass i kjøleskapet med boolean (true/false)
        if(items.size() >= max_size){
            return false; //retunerer false hvis ikke det er plass i kjøleskapet
        }
        return items.add(item); //retunerer true hvis det er plass + item blir lagt til items-listen vår
    }
    @Override
    public void takeOut(FridgeItem item) { //fjerner item fra fridge / sjekker item som skal fjernes
        if(items.size() == 0) { //Hvis det ikke finnes noen item in fridge
            throw new java.util.NoSuchElementException("Fridge is empty"); //En exception vil forekomme
        }
        else if(!items.contains(item)) { //hvis item ikke er i listen
            throw new IllegalArgumentException("The item is not in the fridge");
        }
        items.remove(item); //Fjerner item fra fridge
    }
    @Override
    public void emptyFridge() { //fjerner alle item fra fridge/itemslisten med funksjonen .clear
        items.clear();   
    }
    
    @Override
    public List<FridgeItem> removeExpiredFood() { //Fjerner items som har gått ut på og retunerer en liste av items som har gått ut på dato
        List<FridgeItem> ExpiredItems = new ArrayList<>(); //oppretter en tom liste som heter ExpiredItems som vil inneholde items som er ut på dato
        for(FridgeItem item: items) { //looper gjennom items listen == samme som: for item in items
            if(item.hasExpired()){ //sjekker om den er ut på dato
                ExpiredItems.add((item)); // Hvis ut på dato så legger den til i den tomme ExpiredItems-listen
            }
        }
        items.removeAll(ExpiredItems); //Fjerner all item som er expired fra fridge 
        return ExpiredItems; //retunerer ExpiredItems-listen med items som er expired 
    }
}
